FROM node:alpine as builder

WORKDIR /app

COPY package*.json /app/
RUN npm ci

COPY . .
RUN npm run build

# ---------------------------------------------------
# ---------------------------------------------------
# loosely based on: https://medium.com/@tiangolo/react-in-docker-with-nginx-built-with-multi-stage-docker-builds-including-testing-8cc49d6ec305
FROM nginx:alpine as production

COPY --from=builder /app/build/ /usr/share/nginx/html
COPY ./nginx.conf /etc/nginx/conf.d/default.conf
COPY ./entrypoint.sh ./generate_config_js.sh /

RUN chmod +x entrypoint.sh generate_config_js.sh

# should match the port in nginx.conf
EXPOSE 5000

ENTRYPOINT ["/entrypoint.sh"]
CMD ["nginx", "-g", "daemon off;"]