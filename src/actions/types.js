// Copyright (c) 2021. Michael Kemna.

export const GET_RESORT = 'GET_RESORT';
export const SELECT_RESORT = 'SELECT_RESORT';
export const GET_OVERVIEW = 'GET_OVERVIEW';
export const GET_OVERVIEW_MAX = 'GET_OVERVIEW_MAX';
export const CHANGE_VIEWPORT = 'CHANGE_VIEWPORT';
export const GET_FORECAST = 'GET_FORECAST';
export const GET_FORECAST_PAST = 'GET_FORECAST_PAST';
export const GET_WEATHER = 'GET_WEATHER';

// used for propTypes, please update accordingly
export const ACTION_TYPES = [
  GET_RESORT,
  SELECT_RESORT,
  GET_OVERVIEW,
  GET_OVERVIEW_MAX,
  CHANGE_VIEWPORT,
  GET_FORECAST,
  GET_FORECAST_PAST,
  GET_WEATHER,
];
