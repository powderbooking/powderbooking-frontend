// Copyright (c) 2021. Michael Kemna.

import fetchApi from './dataFetcher';
import { GET_OVERVIEW, GET_OVERVIEW_MAX } from './types';

const getOverviewAction = () => (dispatch) => {
  const overviewPromise = fetchApi(dispatch, '/overview', GET_OVERVIEW);
  const maxPromise = fetchApi(
    dispatch,
    `/overview/max/${window.REACT_APP_SNOW_OR_RAIN}`,
    GET_OVERVIEW_MAX,
  );
  return Promise.all([overviewPromise, maxPromise]);
};

export default getOverviewAction;
