// Copyright (c) 2021. Michael Kemna.

import { SELECT_RESORT } from './types';
import selectResortAction from './selectResort.action';

describe('selectResort', () => {
  test('select resort', () => {
    // arrange
    const resortId = 1;
    const mockedDispatch = jest.fn();

    // act
    selectResortAction(resortId)(mockedDispatch);

    // assert
    expect(mockedDispatch).toBeCalledTimes(1);
    expect(mockedDispatch).toBeCalledWith({
      type: SELECT_RESORT,
      resortId,
    });
  });
});
