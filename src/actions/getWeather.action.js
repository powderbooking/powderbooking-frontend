// Copyright (c) 2021. Michael Kemna.

import fetchApi from './dataFetcher';
import { GET_FORECAST_PAST, GET_WEATHER } from './types';

const getWeatherAction = (resortId) => (dispatch) => {
  const forecastPastPromise = fetchApi(dispatch, `/forecast/past/${resortId}`, GET_FORECAST_PAST);
  const weatherPromise = fetchApi(dispatch, `/weather/${resortId}`, GET_WEATHER);
  return Promise.all([forecastPastPromise, weatherPromise]);
};

export default getWeatherAction;
