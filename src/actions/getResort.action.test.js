// Copyright (c) 2021. Michael Kemna.

import getResortAction from './getResort.action';
import { GET_RESORT } from './types';

describe('getResort actions', () => {
  const id = 1;
  const mockedDispatch = jest.fn();

  beforeEach(() => {
    fetch.resetMocks();
    mockedDispatch.mockReset();
  });

  test('getResort request success', () => {
    // arrange
    const payload = {
      id,
      continent: 'string',
      name: 'string',
      fullname: 'string',
      country: 'string',
      village: 'string',
      lat: 0,
      lng: 0,
      altitude_min_m: 0,
      altitude_max_m: 0,
      lifts: 0,
      slopes_total_km: 0,
      slopes_blue_km: 0,
      slopes_red_km: 0,
      slopes_black_km: 0,
    };
    fetch.mockResponseOnce(JSON.stringify(payload));

    // act
    return getResortAction(id)(mockedDispatch).then(() => {
      // assert
      expect(fetch).toHaveBeenCalledTimes(1);
      expect(fetch).toBeCalledWith('https://powderbooking.com/resort/1');
      expect(mockedDispatch).toHaveBeenCalledTimes(1);
      expect(mockedDispatch).toHaveBeenCalledWith({
        type: GET_RESORT,
        payload,
      });
    });
  });

  test('getResort request error', () => {
    // arrange
    const error = 'API is down';
    fetch.mockReject(() => Promise.reject(error));

    // act
    return getResortAction(id)(mockedDispatch).then(() => {
      // assert
      expect(fetch).toHaveBeenCalledTimes(1);
      expect(fetch).toBeCalledWith('https://powderbooking.com/resort/1');
      expect(mockedDispatch).toBeCalledTimes(1);
      expect(mockedDispatch).toHaveBeenCalledWith({
        type: GET_RESORT,
        error,
      });
    });
  });
});
