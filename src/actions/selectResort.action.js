// Copyright (c) 2021. Michael Kemna.

import PropTypes from 'prop-types';
import { SELECT_RESORT } from './types';

const selectResortAction = (resortId) => (dispatch) => {
  dispatch({
    type: SELECT_RESORT,
    resortId,
  });
};

selectResortAction.propTypes = {
  resortId: PropTypes.number.isRequired,
};

export default selectResortAction;
