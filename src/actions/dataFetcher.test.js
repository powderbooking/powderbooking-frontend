// Copyright (c) 2021. Michael Kemna.

import fetchApi from './dataFetcher';
import { GET_RESORT } from './types';

describe('fetchApi actions', () => {
  const url = '/resort/1';
  const mockedDispatch = jest.fn();

  beforeEach(() => {
    fetch.resetMocks();
    mockedDispatch.mockReset();
  });

  test('request success', () => {
    // arrange
    const payload = { id: 1 };
    fetch.mockResponseOnce(JSON.stringify(payload));

    // act
    return (
      fetchApi(mockedDispatch, url, GET_RESORT)
        // assert
        .then(() => {
          expect(fetch).toHaveBeenCalledTimes(1);
          expect(fetch).toBeCalledWith('https://powderbooking.com/resort/1');
          expect(mockedDispatch).toHaveBeenCalledTimes(1);
          expect(mockedDispatch).toHaveBeenCalledWith({
            type: GET_RESORT,
            payload,
          });
        })
    );
  });

  test('request error', () => {
    // arrange
    const error = 'API is down';
    fetch.mockReject(() => Promise.reject(error));

    // act
    return (
      fetchApi(mockedDispatch, url, GET_RESORT)
        // assert
        .then(() => {
          expect(fetch).toHaveBeenCalledTimes(1);
          expect(fetch).toBeCalledWith('https://powderbooking.com/resort/1');
          expect(mockedDispatch).toHaveBeenCalledTimes(1);
          expect(mockedDispatch).toHaveBeenCalledWith({
            type: GET_RESORT,
            error,
          });
        })
    );
  });
});
