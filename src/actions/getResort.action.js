// Copyright (c) 2021. Michael Kemna.

import PropTypes from 'prop-types';
import fetchApi from './dataFetcher';
import { GET_RESORT } from './types';

const getResortAction = (resortId) => (dispatch) =>
  fetchApi(dispatch, `/resort/${resortId}`, GET_RESORT);

getResortAction.propTypes = {
  resortId: PropTypes.number.isRequired,
};

export default getResortAction;
