// Copyright (c) 2021. Michael Kemna.

import PropTypes from 'prop-types';
import { ACTION_TYPES } from './types';

function fetchApi(dispatch, url, type) {
  return fetch(window.REACT_APP_BACKEND_API + url)
    .then((result) => result.json())
    .then((result) =>
      dispatch({
        type,
        payload: result,
      }),
    )
    .catch((error) => {
      // eslint-disable-next-line no-console
      console.log(`unable to fetch url: ${url}, for type: ${type}.`);
      dispatch({
        type,
        error,
      });
    });
}

fetchApi.propTypes = {
  dispatch: PropTypes.func.isRequired,
  url: PropTypes.string.isRequired,
  type: PropTypes.oneOf(ACTION_TYPES).isRequired,
};

export default fetchApi;
