// Copyright (c) 2021. Michael Kemna.

import PropTypes from 'prop-types';
import fetchApi from './dataFetcher';
import { GET_FORECAST } from './types';

const getForecastAction = (resortId) => (dispatch) =>
  fetchApi(dispatch, `/forecast/current/${resortId}`, GET_FORECAST);

getForecastAction.propTypes = {
  resortId: PropTypes.number.isRequired,
};

export default getForecastAction;
