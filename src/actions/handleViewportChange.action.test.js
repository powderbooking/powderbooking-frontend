// Copyright (c) 2021. Michael Kemna.

import handleViewportChangeAction from './handleViewportChange.action';
import { CHANGE_VIEWPORT } from './types';

describe('viewport changes', () => {
  test('change viewport', () => {
    // arrange
    const newViewport = {
      center: [31.0, 8.0],
      zoom: 3,
    };
    const mockedDispatch = jest.fn();

    // act
    handleViewportChangeAction(newViewport)(mockedDispatch);

    // assert
    expect(mockedDispatch).toBeCalledTimes(1);
    expect(mockedDispatch).toBeCalledWith({
      type: CHANGE_VIEWPORT,
      viewport: newViewport,
    });
  });
});
