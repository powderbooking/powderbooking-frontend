// Copyright (c) 2021. Michael Kemna.

import PropTypes from 'prop-types';
import { CHANGE_VIEWPORT } from './types';

const handleViewportChangeAction = (newViewport) => (dispatch) => {
  dispatch({
    type: CHANGE_VIEWPORT,
    viewport: newViewport,
  });
};

handleViewportChangeAction.propTypes = {
  newViewport: PropTypes.exact({
    center: PropTypes.arrayOf(PropTypes.number).isRequired,
    zoom: PropTypes.number.isRequired,
  }).isRequired,
};

export default handleViewportChangeAction;
