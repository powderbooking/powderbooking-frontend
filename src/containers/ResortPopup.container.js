// Copyright (c) 2021. Michael Kemna.

import { connect } from 'react-redux';
import getResortAction from '../actions/getResort.action';
import Component from '../components/map/resortPopup';

const mapStateToProps = (state, ownProps) => ({
  ...ownProps,
  ...state.resort,
});

const mapDispatchToProps = {
  getResortAction,
};

export default connect(mapStateToProps, mapDispatchToProps)(Component);
