// Copyright (c) 2021. Michael Kemna.

import { connect } from 'react-redux';
import getOverviewAction from '../actions/getOverview.action';
import handleViewportChangeAction from '../actions/handleViewportChange.action';
import Component from '../components/map/Map';

const mapStateToProps = (state) => ({
  data: state.overview.data,
  max: state.overview.max,
});

const mapDispatchToProps = {
  getOverviewAction,
  handleViewportChangeAction,
};

export default connect(mapStateToProps, mapDispatchToProps)(Component);
