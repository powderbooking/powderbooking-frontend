// Copyright (c) 2021. Michael Kemna.

import { connect } from 'react-redux';
import getResortAction from '../actions/getResort.action';
import Component from '../components/resort/Resort';
import selectResortAction from '../actions/selectResort.action';

const mapStateToProps = (state, ownProps) => ({
  resortId: parseInt(ownProps.match.params.resortId, 10),
  resort: state.resort,
});

const mapDispatchToProps = {
  getResortAction,
  selectResortAction,
};

export default connect(mapStateToProps, mapDispatchToProps)(Component);
