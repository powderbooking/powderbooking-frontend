// Copyright (c) 2021. Michael Kemna.

import { connect } from 'react-redux';
import selectResortAction from '../actions/selectResort.action';
import Component from '../components/map/Markers';

const mapStateToProps = (state) => ({
  resortId: state.resort.resortId,
  data: state.overview.data,
  dynamicConfig: state.map.dynamicMarkerConfig,
});

const mapDispatchToProps = {
  selectResortAction,
};

export default connect(mapStateToProps, mapDispatchToProps)(Component);
