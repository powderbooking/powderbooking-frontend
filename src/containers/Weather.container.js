// Copyright (c) 2021. Michael Kemna.

import { connect } from 'react-redux';
import Component from '../components/weather/Weather';
import getWeatherAction from '../actions/getWeather.action';

const mapStateToProps = (state, ownProps) => ({
  ...ownProps,
  ...state.weather,
});

const mapDispatchToProps = {
  getWeatherAction,
};

export default connect(mapStateToProps, mapDispatchToProps)(Component);
