// Copyright (c) 2021. Michael Kemna.

import { connect } from 'react-redux';
import Component from '../components/forecast/Forecast';
import getForecastAction from '../actions/getForecast.action';

const mapStateToProps = (state, ownProps) => ({
  forecast: state.forecast,
  ...ownProps,
});

const mapDispatchToProps = {
  getForecastAction,
};

export default connect(mapStateToProps, mapDispatchToProps)(Component);
