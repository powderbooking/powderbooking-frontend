// Copyright (c) 2021. Michael Kemna.

import overviewReducer from './overview.reducer';
import { GET_OVERVIEW } from '../actions/types';

describe('overview reducers', () => {
  const payload = ['payload'];
  const action = {
    type: GET_OVERVIEW,
    payload,
  };

  test('GET_OVERVIEW, empty state', () => {
    // arrange
    const state = {};

    // act
    const result = overviewReducer(state, action);

    // assert
    expect(result).toStrictEqual({
      data: payload,
    });
  });

  test('GET_OVERVIEW overwrite old state', () => {
    // arrange
    const state = { data: ['old'] };

    // act
    const result = overviewReducer(state, action);

    // assert
    expect(result).toStrictEqual({
      data: payload,
    });
  });

  test('GET_OVERVIEW, other items remain', () => {
    // arrange
    const state = {
      data: [],
      max: 12,
    };

    // act
    const result = overviewReducer(state, action);

    // assert
    expect(result).toStrictEqual({
      data: payload,
      max: 12,
    });
  });
});
