// Copyright (c) 2021. Michael Kemna.

import weatherReducer, { initialState } from './weather.reducer';
import { GET_FORECAST_PAST, GET_WEATHER } from '../actions/types';

describe('weather reducers', () => {
  const payload = { message: 'message' };
  const error = 'errormessage';
  const actionForecastPast = {
    type: GET_FORECAST_PAST,
    payload,
  };
  const actionWeather = {
    type: GET_WEATHER,
    payload,
  };

  test('GET_FORECAST_PAST success', () => {
    // arrange
    // act
    const result = weatherReducer(initialState, actionForecastPast);

    // assert
    expect(result.isLoaded).toBe(false);
    expect(result.forecastPast.isLoaded).toBe(true);
    expect(result.forecastPast.payload).toStrictEqual(payload);
  });

  test('GET_WEATHER success', () => {
    // arrange
    // act
    const result = weatherReducer(initialState, actionWeather);

    // assert
    expect(result.isLoaded).toBe(false);
    expect(result.weather.isLoaded).toBe(true);
    expect(result.weather.payload).toStrictEqual(payload);
  });

  test('GET_FORECAST_PAST into GET_WEATHER success', () => {
    // arrange
    // act
    const result = weatherReducer(weatherReducer(initialState, actionForecastPast), actionWeather);

    // assert
    expect(result.isLoaded).toBe(true);
  });

  test('GET_FORECAST_PAST fail', () => {
    // arrange
    const action = {
      type: GET_FORECAST_PAST,
      error,
    };

    // act
    const result = weatherReducer(initialState, action);

    // assert
    expect(result.isLoaded).toBe(false);
    expect(result.forecastPast.isLoaded).toBe(true);
    expect(result.forecastPast.error).toStrictEqual(error);
  });

  test('GET_WEATHER fail', () => {
    // arrange
    const action = {
      type: GET_WEATHER,
      error,
    };

    // act
    const result = weatherReducer(initialState, action);

    // assert
    expect(result.isLoaded).toBe(false);
    expect(result.weather.isLoaded).toBe(true);
    expect(result.weather.error).toStrictEqual(error);
  });
});
