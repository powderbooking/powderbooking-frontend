// Copyright (c) 2021. Michael Kemna.

import { combineReducers } from 'redux';
import overview from './overview.reducer';
import resort from './resort.reducer';
import map from './map.reducer';
import forecast from './forecast.reducer';
import weather from './weather.reducer';

export default combineReducers({ overview, resort, map, forecast, weather });
