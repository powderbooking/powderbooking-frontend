// Copyright (c) 2021. Michael Kemna.

import { GET_FORECAST_PAST, GET_WEATHER } from '../actions/types';

export const initialState = {
  isLoaded: false, // if both weather and forecastPast are loaded, this is true
  weather: {
    isLoaded: false,
    error: null,
    payload: null,
  },
  forecastPast: {
    isLoaded: false,
    error: null,
    payload: null,
  },
};

function weatherReducer(state = initialState, action) {
  let result;
  switch (action.type) {
    case GET_FORECAST_PAST:
      result = action.payload ? { payload: action.payload } : { error: action.error };
      return {
        ...state,
        isLoaded: state.weather.isLoaded,
        forecastPast: {
          isLoaded: true,
          ...result,
        },
      };
    case GET_WEATHER:
      result = action.payload ? { payload: action.payload } : { error: action.error };
      return {
        ...state,
        isLoaded: state.forecastPast.isLoaded,
        weather: {
          isLoaded: true,
          ...result,
        },
      };
    default:
      return state;
  }
}

export default weatherReducer;
