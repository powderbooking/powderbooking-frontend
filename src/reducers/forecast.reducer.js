// Copyright (c) 2021. Michael Kemna.

import _ from 'lodash';
import { GET_FORECAST } from '../actions/types';

const initialState = {
  resortId: null,
  error: null,
  isLoaded: false,
  payload: {},
  tempMin: [],
  tempMax: [],
  dates: [],
  snow: [],
  rain: [],
};

function forecastReducer(state = initialState, action) {
  switch (action.type) {
    case GET_FORECAST:
      // case: an error occurred
      if (!action.payload) {
        return {
          ...state,
          error: action.error,
          isLoaded: true,
        };
      }
      // case: successful, let's unwrap the data
      return {
        resortId: parseInt(_.get(action.payload, '[0].resort_id'), 10),
        error: null,
        isLoaded: true,
        payload: action.payload,
        tempMin: action.payload.map((row) => parseFloat(_.get(row, 'temperature_min_c'))),
        tempMax: action.payload.map((row) => parseFloat(_.get(row, 'temperature_max_c'))),
        snow: action.payload.map((row) => parseFloat(_.get(row, 'snow_total_mm'))),
        rain: action.payload.map((row) => parseFloat(_.get(row, 'rain_total_mm'))),
        dates: action.payload.map((row) => _.get(row, 'date')),
      };
    default:
      return state;
  }
}

export default forecastReducer;
