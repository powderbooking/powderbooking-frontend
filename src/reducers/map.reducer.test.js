// Copyright (c) 2021. Michael Kemna.

import each from 'jest-each';
import mapReducer, { convertZoomToMarkerConfig } from './map.reducer';
import { CHANGE_VIEWPORT } from '../actions/types';

describe('convertZoomToMarkerConfig', () => {
  each([
    [1, 0.0],
    [7, 0.0],
    [8, 0.15],
    [9, 0.15],
    [10, 0.5],
    [12, 0.5],
  ]).test("when zoom is '%s'", (zoom, expectedOpacity) => {
    // arrange
    // act
    const result = convertZoomToMarkerConfig(zoom);

    // assert
    expect(result.radius).toBe(10);
    expect(result.opacity).toBe(expectedOpacity);
    expect(result.fillOpacity).toBe(expectedOpacity * 0.6);
  });
});

describe('map reducer', () => {
  const initialState = {
    viewport: {
      center: [31.0, 8.0],
      zoom: 3,
    },
    dynamicMarkerConfig: 1,
  };

  test('same zoom, different viewport', () => {
    // arrange
    const newViewport = {
      center: [3.0, 3.0],
      zoom: 3,
    };
    const action = {
      type: CHANGE_VIEWPORT,
      viewport: newViewport,
    };

    // act
    const result = mapReducer(initialState, action);

    // assert
    expect(result.dynamicMarkerConfig).toBe(initialState.dynamicMarkerConfig);
    expect(result.viewport).toBe(newViewport);
  });

  test('different zoom', () => {
    // arrange
    const newViewport = {
      center: [3.0, 3.0],
      zoom: 5,
    };
    const action = {
      type: CHANGE_VIEWPORT,
      viewport: newViewport,
    };

    // act
    const result = mapReducer(initialState, action);

    // assert
    expect(result.dynamicMarkerConfig).toStrictEqual(convertZoomToMarkerConfig(newViewport.zoom));
    expect(result.viewport).toBe(newViewport);
  });
});
