// Copyright (c) 2021. Michael Kemna.

import { GET_OVERVIEW, GET_OVERVIEW_MAX } from '../actions/types';

export const initialState = {
  data: [],
  max: 0,
};

function overviewReducer(state = initialState, action) {
  switch (action.type) {
    case GET_OVERVIEW:
      return {
        ...state,
        data: action.payload,
      };
    case GET_OVERVIEW_MAX:
      return {
        ...state,
        max: action.payload.max,
      };
    default:
      return state;
  }
}

export default overviewReducer;
