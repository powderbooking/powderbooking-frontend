// Copyright (c) 2021. Michael Kemna.

import { GET_RESORT, SELECT_RESORT } from '../actions/types';
import resortReducer from './resort.reducer';

describe('resort reducers', () => {
  const resortId = 1;

  test('SELECT_RESORT', () => {
    // arrange
    const initialState = {
      resortId: null,
      error: null,
      isLoaded: false,
      payload: {},
    };
    const action = {
      type: SELECT_RESORT,
      resortId,
    };

    // act
    const newState = resortReducer(initialState, action);

    // assert
    expect(newState).toEqual({
      resortId,
      error: null,
      isLoaded: false,
      payload: {},
    });
  });

  const initialState = {
    resortId,
    error: null,
    isLoaded: false,
    payload: {},
  };

  test('GET_RESORT success', () => {
    // arrange
    const payload = {
      id: resortId,
      continent: 'string',
      name: 'string',
      fullname: 'string',
      country: 'string',
      village: 'string',
      lat: 0,
      lng: 0,
      altitude_min_m: 0,
      altitude_max_m: 0,
      lifts: 0,
      slopes_total_km: 0,
      slopes_blue_km: 0,
      slopes_red_km: 0,
      slopes_black_km: 0,
    };
    const action = {
      type: GET_RESORT,
      payload,
    };

    // act
    const newState = resortReducer(initialState, action);

    // assert
    expect(newState).toEqual({
      resortId,
      error: null,
      isLoaded: true,
      payload,
    });
  });

  test('GET_RESORT fail', () => {
    // arrange
    const error = { error: 'error' };
    const action = {
      type: GET_RESORT,
      error,
    };

    // act
    const newState = resortReducer(initialState, action);

    // assert
    expect(newState).toEqual({
      resortId,
      error,
      isLoaded: true,
      payload: {},
    });
  });
});
