// Copyright (c) 2021. Michael Kemna.

import { GET_RESORT, SELECT_RESORT } from '../actions/types';

const initialState = {
  resortId: null,
  error: null,
  isLoaded: false,
  payload: {},
};

function resortReducer(state = initialState, action) {
  switch (action.type) {
    case GET_RESORT:
      return action.payload
        ? {
            ...state,
            resortId: action.payload.id,
            isLoaded: true,
            payload: action.payload,
          }
        : {
            ...state,
            isLoaded: true,
            error: action.error,
          };
    case SELECT_RESORT:
      return {
        ...initialState, // reset state because we are looking for a new resort
        resortId: action.resortId,
      };
    default:
      return state;
  }
}

export default resortReducer;
