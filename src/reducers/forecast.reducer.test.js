// Copyright (c) 2021. Michael Kemna.

import { GET_FORECAST } from '../actions/types';
import forecastReducer from './forecast.reducer';

describe('forecast reducer', () => {
  const initialState = {
    resortId: null,
    error: null,
    isLoaded: false,
    payload: {},
    tempMin: [],
    tempMax: [],
    dates: [],
    snow: [],
    rain: [],
  };

  test('GET_FORECAST success', () => {
    // arrange
    const payload = [
      {
        id: 0,
        resortId: 0,
        date_request: '2020-12-24T13:07:07.584Z',
        date: '2020-12-24',
        timepoint: 0,
        temperature_max_c: 10,
        temperature_min_c: 20,
        rain_total_mm: 30,
        snow_total_mm: 40,
        prob_precip_pct: 0,
        wind_speed_max_kmh: 0,
        windgst_max_kmh: 0,
      },

      {
        id: 0,
        resortId: 0,
        date_request: '2020-12-24T13:07:07.584Z',
        date: '2020-12-25',
        timepoint: 1,
        temperature_max_c: 11,
        temperature_min_c: 21,
        rain_total_mm: 31,
        snow_total_mm: 41,
        prob_precip_pct: 0,
        wind_speed_max_kmh: 0,
        windgst_max_kmh: 0,
      },
    ];
    const action = {
      type: GET_FORECAST,
      payload,
    };

    // act
    const result = forecastReducer(initialState, action);

    // assert
    expect(result.isLoaded).toBe(true);
    expect(result.payload).toStrictEqual(payload);
    expect(result.tempMax).toStrictEqual([10, 11]);
    expect(result.tempMin).toStrictEqual([20, 21]);
    expect(result.dates).toStrictEqual(['2020-12-24', '2020-12-25']);
    expect(result.snow).toStrictEqual([40, 41]);
    expect(result.rain).toStrictEqual([30, 31]);
  });

  test('GET_FORECAST fail', () => {
    // arrange
    const error = 'API failure';
    const action = {
      type: GET_FORECAST,
      error,
    };

    // act
    const result = forecastReducer(initialState, action);

    // assert
    expect(result.isLoaded).toBe(true);
    expect(result.error).toBe(error);
  });
});
