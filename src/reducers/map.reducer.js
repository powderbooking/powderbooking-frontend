// Copyright (c) 2021. Michael Kemna.

import { CHANGE_VIEWPORT } from '../actions/types';

export function convertZoomToMarkerConfig(zoom) {
  const radius = 10;
  let opacity;
  if (zoom < 8) {
    opacity = 0.0;
  } else if (zoom < 10) {
    opacity = 0.15;
  } else {
    opacity = 0.5;
  }

  return {
    radius,
    opacity,
    fillOpacity: opacity * 0.6,
  };
}

const initialState = {
  viewport: {
    center: [31.0, 8.0],
    zoom: 3,
  },
  dynamicMarkerConfig: convertZoomToMarkerConfig(3),
};

function mapReducer(state = initialState, action) {
  switch (action.type) {
    case CHANGE_VIEWPORT:
      return {
        ...state,
        viewport: action.viewport,
        dynamicMarkerConfig:
          action.viewport.zoom === state.viewport.zoom
            ? state.dynamicMarkerConfig
            : convertZoomToMarkerConfig(action.viewport.zoom),
      };
    default:
      return state;
  }
}

export default mapReducer;
