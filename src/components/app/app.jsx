// Copyright (c) 2021. Michael Kemna.

import React from 'react';
import { BrowserRouter as Router, Redirect, Route, Switch } from 'react-router-dom';
import Navbar from '../navbar/navbar';
import Explanation from '../explanation/explanation';
import About from '../about/about';
import routes from '../../routes';

function app() {
  return (
    <>
      <Router>
        <Navbar links={routes} />
        <div id="content" className="container-fluid">
          <Switch>
            {routes
              .filter((route) => route.isRoute)
              .map((route) => {
                return (
                  <Route key={route.id.toString()} path={route.url} component={route.component} />
                );
              })}
            <Route key="default">
              <Redirect to="/map" />
            </Route>
          </Switch>
        </div>
      </Router>
      <Explanation />
      <About />
    </>
  );
}

export default app;
