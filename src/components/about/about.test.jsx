// Copyright (c) 2021. Michael Kemna.

import React from 'react';
import { render, unmountComponentAtNode } from 'react-dom';
import { act } from 'react-dom/test-utils';

import About from './about';

let container = null;
beforeEach(() => {
  // setup a DOM element as a render target
  container = document.createElement('div');
  document.body.appendChild(container);
});

afterEach(() => {
  // cleanup on exiting
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

it('renders with me on the right', () => {
  act(() => {
    render(<About />, container);
  });
  expect(container.textContent).toContain('This is me on the right!');
});
