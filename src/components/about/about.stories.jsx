// Copyright (c) 2021. Michael Kemna.

import React from 'react';
import About from './about';

export default {
  title: 'Section/About',
  component: About,
};

export const standard = () => <About />;
