// Copyright (c) 2021. Michael Kemna.

import React from 'react';
import './style.css';
import PropTypes from 'prop-types';

function ResortDetails({ resort }) {
  return (
    <div className="row flex-column">
      <h6>
        <b>Resort overview</b>
      </h6>
      <p>
        <b>Altitude:</b> {resort.altitude_min_m} - {resort.altitude_max_m} m
      </p>
      <p>
        <b>Total size:</b>
        <span className="p-1"> {resort.slopes_total_km} km </span>
      </p>
      <p>
        <b>Slopes:</b>
        <span className="p-1 bg-primary text-white"> {resort.slopes_blue_km} km </span>
        <span className="p-1 bg-danger text-white"> {resort.slopes_red_km} km </span>
        <span className="p-1 bg-dark text-white"> {resort.slopes_black_km} km </span>
      </p>
      <p>
        <b>Amount of lifts:</b> {resort.lifts}
      </p>
    </div>
  );
}

ResortDetails.propTypes = {
  resort: PropTypes.shape({
    id: PropTypes.number,
    continent: PropTypes.string,
    name: PropTypes.string,
    fullname: PropTypes.string,
    country: PropTypes.string,
    village: PropTypes.string,
    lat: PropTypes.number,
    lng: PropTypes.number,
    altitude_min_m: PropTypes.number,
    altitude_max_m: PropTypes.number,
    lifts: PropTypes.number,
    slopes_total_km: PropTypes.number,
    slopes_blue_km: PropTypes.number,
    slopes_red_km: PropTypes.number,
    slopes_black_km: PropTypes.number,
  }).isRequired,
};

export default ResortDetails;
