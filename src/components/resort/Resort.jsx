// Copyright (c) 2021. Michael Kemna.

import React from 'react';
import './style.css';
import PropTypes from 'prop-types';
import ResortDetails from './resortDetails';
import Forecast from '../../containers/Forecast.container';
import Weather from '../../containers/Weather.container';

class Resort extends React.Component {
  componentDidMount() {
    const { resortId, resort, getResortAction } = this.props;
    if (!resort.resortId || !resort.isLoaded || resort.resortId !== resortId) {
      // TODO: also reset state to avoid possible wrong data to be shown? Super edge case..
      getResortAction(resortId);
    }
    window.scrollTo(0, 0);
  }

  render() {
    const { resortId, resort } = this.props;
    const { error, isLoaded, payload } = resort;
    const { village, continent, country } = payload;
    if (error) {
      return <div>Error: {error.message}</div>;
    }
    if (!isLoaded) {
      return <div>Loading...</div>;
    }
    return (
      <div className="container">
        <div id="current_resort" className="row flex-column">
          <h1>{village}</h1>
          <h5>
            {continent} / {country}
          </h5>
          &nbsp;
          <ResortDetails resort={resort.payload} />
        </div>
        <Weather resortId={resortId} />
        <Forecast resortId={resortId} />
      </div>
    );
  }
}

Resort.propTypes = {
  resortId: PropTypes.number.isRequired,
  resort: PropTypes.exact({
    resortId: PropTypes.number,
    error: PropTypes.string,
    isLoaded: PropTypes.bool,
    payload: PropTypes.exact({
      id: PropTypes.number,
      continent: PropTypes.string,
      name: PropTypes.string,
      fullname: PropTypes.string,
      country: PropTypes.string,
      village: PropTypes.string,
      lat: PropTypes.number,
      lng: PropTypes.number,
      altitude_min_m: PropTypes.number,
      altitude_max_m: PropTypes.number,
      lifts: PropTypes.number,
      slopes_total_km: PropTypes.number,
      slopes_blue_km: PropTypes.number,
      slopes_red_km: PropTypes.number,
      slopes_black_km: PropTypes.number,
    }),
  }).isRequired,
  getResortAction: PropTypes.func.isRequired,
};
export default Resort;
