// Copyright (c) 2021. Michael Kemna.

import React from 'react';
import { Line } from 'react-chartjs-2';
import PropTypes from 'prop-types';
import chartOptions from './chartOptions';

function ChartTemp({ tempMax, tempMin, labels }) {
  // source: https://github.com/jerairrest/react-chartjs-2#getting-context-for-data-generation
  const data = (canvas) => {
    const ctx = canvas.getContext('2d');
    const gradientMax = ctx.createLinearGradient(0, 200, 0, 0);
    gradientMax.addColorStop(0, 'rgba(255,38,26,1)');
    gradientMax.addColorStop(0.1, 'rgba(255,38,26,0.5)');
    gradientMax.addColorStop(1, 'rgba(255,38,26,0)');

    const gradientMin = ctx.createLinearGradient(0, 0, 0, 300);
    gradientMin.addColorStop(0, 'rgba(0,123,255,1)');
    gradientMin.addColorStop(0.1, 'rgba(0,123,255,0.5)');
    gradientMin.addColorStop(1, 'rgba(0,123,255,0)');

    return {
      labels,
      datasets: [
        {
          label: 'Freezing point',
          data: [...Array(labels.length).fill(0)],
          borderColor: '#eeeeee',
          borderWidth: 2,
          borderDash: [5, 5],
          pointRadius: 0,
        },
        {
          label: 'Maximum temperature (C)',
          data: tempMax,
          fill: 'end',
          backgroundColor: gradientMax,
          borderColor: ['#ff261a'],
          borderWidth: 4,
          pointRadius: 0,
          pointHitRadius: 10,
        },
        {
          label: 'Minimum temperature (C)',
          data: tempMin,
          fill: 'start',
          backgroundColor: gradientMin,
          borderColor: ['#007BFF'],
          borderWidth: 4,
          pointRadius: 0,
          pointHitRadius: 10,
        },
      ],
    };
  };

  return <Line data={data} width={400} height={100} options={chartOptions} />;
}

ChartTemp.propTypes = {
  /**
   * An array of max temperature, measurement placement should correspond with the label.
   */
  tempMax: PropTypes.arrayOf(PropTypes.number).isRequired,
  /**
   * An array of min temperature, measurement placement should correspond with the label.
   */
  tempMin: PropTypes.arrayOf(PropTypes.number).isRequired,
  /**
   * An array of corresponding labels.
   */
  labels: PropTypes.arrayOf(PropTypes.string).isRequired,
};

export default ChartTemp;
