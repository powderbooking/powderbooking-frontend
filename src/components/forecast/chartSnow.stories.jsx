// Copyright (c) 2021. Michael Kemna.

import React from 'react';
import ChartSnow from './chartSnow';

export default {
  title: 'Chart/Snow',
  component: ChartSnow,
};

const Template = (args) => <ChartSnow {...args} />;

const labels = [
  '01-01-2021',
  '02-01-2021',
  '03-01-2021',
  '04-01-2021',
  '05-01-2021',
  '06-01-2021',
  '07-01-2021',
];

export const Straight = Template.bind({});
Straight.args = {
  snow: [5, 5, 5, 5, 5, 5, 5],
  rain: [1, 1, 1, 1, 1, 1, 1],
  labels,
};

export const Wave = Template.bind({});
Wave.args = {
  snow: [1, 3, 5, 3, 1, 3, 5],
  rain: [1, 1, 1, 1, 1, 1, 1],
  labels,
};

export const Waves = Template.bind({});
Waves.args = {
  snow: [1, 3, 5, 3, 1, 3, 5],
  rain: [1, 0, 1, 0, 1, 0, 1],
  labels,
};

export const SnowOnly = Template.bind({});
SnowOnly.args = {
  snow: [1, 3, 5, 3, 1, 3, 5],
  rain: [0, 0, 0, 0, 0, 0, 0],
  labels,
};

export const RainOnly = Template.bind({});
RainOnly.args = {
  snow: [0, 0, 0, 0, 0, 0, 0],
  rain: [1, 3, 5, 3, 1, 3, 5],
  labels,
};
