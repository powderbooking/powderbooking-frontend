// Copyright (c) 2021. Michael Kemna.

import React from 'react';
import ChartTemp from './chartTemp';

export default {
  title: 'Chart/Temp',
  component: ChartTemp,
};

const Template = (args) => <ChartTemp {...args} />;

const labels = [
  '01-01-2021',
  '02-01-2021',
  '03-01-2021',
  '04-01-2021',
  '05-01-2021',
  '06-01-2021',
  '07-01-2021',
];

export const Straight = Template.bind({});
Straight.args = {
  tempMax: [5, 5, 5, 5, 5, 5, 5],
  tempMin: [-3, -3, -3, -3, -3, -3, -3],
  labels,
};

export const Wave = Template.bind({});
Wave.args = {
  tempMax: [1, 3, 5, 3, 1, 3, 5],
  tempMin: [-3, -3, -3, -3, -3, -3, -3],
  labels,
};

export const Waves = Template.bind({});
Waves.args = {
  tempMax: [1, 3, 5, 3, 1, 3, 5],
  tempMin: [-4, -3, -4, -3, -4, -3, -4],
  labels,
};
