// Copyright (c) 2021. Michael Kemna.

import React from 'react';
import { Line } from 'react-chartjs-2';
import PropTypes from 'prop-types';
import chartOptions from './chartOptions';

function ChartSnow({ snow, rain, labels }) {
  // source: https://github.com/jerairrest/react-chartjs-2#getting-context-for-data-generation
  const data = (canvas) => {
    const ctx = canvas.getContext('2d');
    const gradientSnow = ctx.createLinearGradient(0, 0, 0, 200);
    gradientSnow.addColorStop(0, 'rgba(238,238,238,1)');
    gradientSnow.addColorStop(0.25, 'rgba(238,238,238,0.5)');
    gradientSnow.addColorStop(1, 'rgba(238,238,238,0)');

    const gradientRain = ctx.createLinearGradient(0, 0, 0, 200);
    gradientRain.addColorStop(0, 'rgba(0,123,255,0.5)');
    gradientRain.addColorStop(0.1, 'rgba(0,123,255,0.33)');
    gradientRain.addColorStop(1, 'rgba(0,123,255,0)');

    return {
      labels,
      datasets: [
        {
          label: 'Zero line',
          data: [...Array(labels.length).fill(0)],
          borderColor: '#eeeeee',
          borderWidth: 2,
          borderDash: [5, 5],
          pointRadius: 0,
        },
        {
          label: 'Expected snow fall (in mm)',
          data: snow,
          fill: 'origin',
          backgroundColor: gradientSnow,
          // backgroundColor: ['#ffffff'],
          borderColor: ['#ffffff'],
          borderWidth: 4,
          pointRadius: 0,
          pointHitRadius: 10,
        },
        {
          label: 'Expected rain fall (in mm)',
          data: rain,
          fill: 'origin',
          backgroundColor: gradientRain,
          borderColor: ['#007BFF'],
          borderWidth: 2,
          pointRadius: 0,
          pointHitRadius: 10,
        },
      ],
    };
  };

  return <Line data={data} width={400} height={100} options={chartOptions} />;
}

ChartSnow.propTypes = {
  /**
   * An array of snow results, measurement placement should correspond with the label.
   */
  snow: PropTypes.arrayOf(PropTypes.number).isRequired,
  /**
   * An array of rain results, measurement placement should correspond with the label.
   */
  rain: PropTypes.arrayOf(PropTypes.number).isRequired,
  /**
   * An array of corresponding labels.
   */
  labels: PropTypes.arrayOf(PropTypes.string).isRequired,
};

export default ChartSnow;
