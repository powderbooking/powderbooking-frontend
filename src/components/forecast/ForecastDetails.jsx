// Copyright (c) 2021. Michael Kemna.

import React from 'react';
import './style.css';
import _ from 'lodash';
import PropTypes from 'prop-types';

class ForecastDetails extends React.Component {
  componentDidMount() {
    const { resortId, forecast, getForecastAction } = this.props;
    if (!forecast.resortId || !forecast.isLoaded || forecast.resortId !== resortId) {
      getForecastAction(resortId);
    }
  }

  render() {
    const { forecast } = this.props;
    const { error, isLoaded, payload } = forecast;
    const forecastToday = payload[0];
    if (error) {
      return <div>Error: {error.message}</div>;
    }
    if (!isLoaded) {
      return <div>Loading...</div>;
    }
    return (
      <div className="col-md-6">
        <div className="row flex-column">
          <h6>
            <b>Weather forecast summary</b>
          </h6>
          <p>
            <b>Snow:</b> {_.get(forecastToday, 'snow_total_mm')} mm
          </p>
          <p>
            <b>Rain:</b> {_.get(forecastToday, 'rain_total_mm')} mm
          </p>
          <p>
            <b>Temperature:</b> {_.get(forecastToday, 'temperature_min_c')} -{' '}
            {_.get(forecastToday, 'temperature_max_c')} &deg;C
          </p>
          <p>
            <b>wind (max):</b> {_.get(forecastToday, 'wind_speed_max_kmh')} km/h
          </p>
        </div>
      </div>
    );
  }
}

ForecastDetails.propTypes = {
  resortId: PropTypes.number.isRequired,
  forecast: PropTypes.shape({
    resortId: PropTypes.number,
    error: PropTypes.string,
    isLoaded: PropTypes.bool.isRequired,
    // TODO: I am at a complete loss here.. It's telling me forecast.payload is an object..
    // eslint-disable-next-line react/forbid-prop-types
    payload: PropTypes.any,
    // payload: PropTypes.arrayOf(
    //   PropTypes.shape({
    //     id: PropTypes.number,
    //     resort_id: PropTypes.number,
    //     date_request: PropTypes.string,
    //     date: PropTypes.string,
    //     timepoint: PropTypes.number,
    //     temperature_max_c: PropTypes.number,
    //     temperature_min_c: PropTypes.number,
    //     rain_total_mm: PropTypes.number,
    //     snow_total_mm: PropTypes.number,
    //     prob_precip_pct: PropTypes.number,
    //     wind_speed_max_kmh: PropTypes.number,
    //     windgst_max_kmh: PropTypes.number,
    //   }),
    // ).isRequired,
  }).isRequired,
  getForecastAction: PropTypes.func.isRequired,
};
export default ForecastDetails;
