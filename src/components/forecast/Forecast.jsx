// Copyright (c) 2021. Michael Kemna.

import React from 'react';
import './style.css';
import _ from 'lodash';
import PropTypes from 'prop-types';
import ChartTemp from './chartTemp';
import ChartSnow from './chartSnow';

class Forecast extends React.Component {
  componentDidMount() {
    const { resortId, forecast, getForecastAction } = this.props;
    if (!forecast.resortId || !forecast.isLoaded || forecast.resortId !== resortId) {
      getForecastAction(resortId);
    }
  }

  render() {
    const { forecast } = this.props;
    const { error, isLoaded, dates, tempMin, tempMax, snow, rain } = forecast;
    if (error) {
      return <div>Error: {error.message}</div>;
    }
    if (!isLoaded) {
      return <div>Loading...</div>;
    }
    return (
      <div id="forecast" className="row flex-column">
        <h1>Weather forecast </h1>
        <p>
          <i>Last updated on {_.get(dates, '[0]')}</i>
        </p>

        <h4>Temperature (&deg;C)</h4>
        <ChartTemp labels={dates} tempMin={tempMin} tempMax={tempMax} />

        <h4>Snow and rain fall (in mm)</h4>
        <ChartSnow labels={dates} snow={snow} rain={rain} />
      </div>
    );
  }
}

Forecast.propTypes = {
  resortId: PropTypes.number.isRequired,
  forecast: PropTypes.shape({
    resortId: PropTypes.number,
    error: PropTypes.string,
    isLoaded: PropTypes.bool.isRequired,
    snow: PropTypes.arrayOf(PropTypes.number).isRequired,
    rain: PropTypes.arrayOf(PropTypes.number).isRequired,
    tempMax: PropTypes.arrayOf(PropTypes.number).isRequired,
    tempMin: PropTypes.arrayOf(PropTypes.number).isRequired,
    dates: PropTypes.arrayOf(PropTypes.string).isRequired,
  }).isRequired,
  getForecastAction: PropTypes.func.isRequired,
};

export default Forecast;
