// Copyright (c) 2021. Michael Kemna.

import { defaults } from 'react-chartjs-2';

defaults.global.defaultFontColor = '#eeeeee';

const chartOptions = {
  legend: { display: false },
  scales: {
    yAxes: [
      {
        ticks: {
          maxTicksLimit: 5,
        },
        gridLines: {
          color: '#5f5f61',
        },
      },
    ],
    xAxes: [
      {
        ticks: {},
        gridLines: {
          display: false,
          drawTicks: true,
        },
      },
    ],
  },
};

export default chartOptions;
