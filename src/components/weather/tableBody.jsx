// Copyright (c) 2021. Michael Kemna.

import React from 'react';
import _ from 'lodash';
import './style.css';
import PropTypes from 'prop-types';

const celsius = String.fromCharCode(0x2103);
const lte = `    ${String.fromCharCode(0x2264)}`;
const gte = `    ${String.fromCharCode(0x2265)}`;

function annotateAfter(value, symbol) {
  return value || value === 0 ? `${value} ${symbol}` : null;
}

annotateAfter.propTypes = {
  value: PropTypes.string.isRequired,
  symbol: PropTypes.string.isRequired,
};

function annotateBefore(value, symbol) {
  return value || value === 0 ? `${symbol} ${value}` : null;
}

annotateBefore.propTypes = {
  value: PropTypes.string.isRequired,
  symbol: PropTypes.string.isRequired,
};

function TableBody({ weather, forecastPast }) {
  return (
    <tbody>
      <tr className="table-row">
        <td rowSpan="2" className="title-row">
          <h5>max</h5>
          <h4>
            <b>Temperature</b>
          </h4>
          <h5>min</h5>
        </td>
        <td rowSpan="2" className="curr">
          <h4>
            <b>{annotateAfter(_.get(weather, 'temperature_c', '??'), celsius)}</b>
          </h4>
        </td>
        <td rowSpan="2" />
        <td className="prev-1">
          {annotateBefore(_.get(forecastPast, '[0].temperature_max_c'), lte)}
        </td>
        <td className="prev-3">
          {annotateBefore(_.get(forecastPast, '[2].temperature_max_c'), lte)}
        </td>
        <td className="prev-7">
          {annotateBefore(_.get(forecastPast, '[6].temperature_max_c'), lte)}
        </td>
      </tr>
      <tr className="table-row">
        <td className="prev-1">
          {annotateBefore(_.get(forecastPast, '[0].temperature_min_c'), gte)}
        </td>
        <td className="prev-3">
          {annotateBefore(_.get(forecastPast, '[2].temperature_min_c'), gte)}
        </td>
        <td className="prev-7">
          {annotateBefore(_.get(forecastPast, '[6].temperature_min_c'), gte)}
        </td>
      </tr>
      <tr className="table-row">
        <td colSpan="6" />
      </tr>
      <tr className="table-row">
        <td className="title-row" title="Amount of snow fallen in the last 24 hours">
          <h4>
            <b>Snowfall</b>
          </h4>
        </td>
        <td className="curr">
          <h4>
            <b>{annotateAfter(_.get(forecastPast, '[0].snow_total_mm', '??'), 'mm')}</b>
          </h4>
        </td>
        <td />
        <td className="prev-1">{_.get(forecastPast, '[0].snow_total_mm')}</td>
        <td className="prev-3">{_.get(forecastPast, '[2].snow_total_mm')}</td>
        <td className="prev-7">{_.get(forecastPast, '[6].snow_total_mm')}</td>
      </tr>
      <tr className="table-row">
        <td className="title-row">
          <h5>last 3h</h5>
        </td>
        <td className="curr">{annotateAfter(_.get(weather, 'snow_3h_mm', '??'), 'mm')}</td>
        <td colSpan="4" />
      </tr>
      <tr className="table-row">
        <td colSpan="6" />
      </tr>
      <tr className="table-row">
        <td className="title-row" title="Amount of rain fallen in the last 24 hours">
          <h4>
            <b>Rainfall</b>
          </h4>
        </td>
        <td className="curr">
          <h4>
            <b>{annotateAfter(_.get(forecastPast, '[0].rain_total_mm', '??'), 'mm')}</b>
          </h4>
        </td>
        <td />
        <td className="prev-1">{_.get(forecastPast, '[0].rain_total_mm')}</td>
        <td className="prev-3">{_.get(forecastPast, '[2].rain_total_mm')}</td>
        <td className="prev-7">{_.get(forecastPast, '[6].rain_total_mm')}</td>
      </tr>
      <tr className="table-row">
        <td className="title-row">
          <h5>last 3h</h5>
        </td>
        <td className="curr">{annotateAfter(_.get(weather, 'rain_3h_mm', '??'), 'mm')}</td>
        <td colSpan="4" />
      </tr>
      <tr className="table-row">
        <td colSpan="6" />
      </tr>
      <tr className="table-row">
        <td className="title-row">
          <h4>
            <b>Windspeed</b>
          </h4>
        </td>
        <td className="curr">
          <h4>
            <b>{annotateAfter(_.get(weather, 'wind_speed_kmh', '??'), 'km/h')}</b>
          </h4>
        </td>
        <td />
        <td className="prev-1">
          {annotateBefore(_.get(forecastPast, '[0].wind_speed_max_kmh'), lte)}
        </td>
        <td className="prev-3">
          {annotateBefore(_.get(forecastPast, '[2].wind_speed_max_kmh'), lte)}
        </td>
        <td className="prev-7">
          {annotateBefore(_.get(forecastPast, '[6].wind_speed_max_kmh'), lte)}
        </td>
      </tr>
      {/* TODO: add compass */}
    </tbody>
  );
}

TableBody.propTypes = {
  weather: PropTypes.shape({
    id: PropTypes.number,
    resort_id: PropTypes.number,
    date_request: PropTypes.string,
    dt: PropTypes.string,
    date: PropTypes.string,
    timepoint: PropTypes.number,
    temperature_c: PropTypes.number,
    wind_speed_kmh: PropTypes.number,
    wind_direction_deg: PropTypes.number,
    visibility_km: PropTypes.number,
    clouds_pct: PropTypes.number,
    snow_3h_mm: PropTypes.number,
    rain_3h_mm: PropTypes.number,
  }).isRequired,
  forecastPast: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number,
      resort_id: PropTypes.number,
      date_request: PropTypes.string,
      date: PropTypes.string,
      timepoint: PropTypes.number,
      temperature_max_c: PropTypes.number,
      temperature_min_c: PropTypes.number,
      rain_total_mm: PropTypes.number,
      snow_total_mm: PropTypes.number,
      prob_precip_pct: PropTypes.number,
      wind_speed_max_kmh: PropTypes.number,
      windgst_max_kmh: PropTypes.number,
    }),
  ).isRequired,
};

export default TableBody;
