// Copyright (c) 2021. Michael Kemna.

import React from 'react';
import './style.css';
import _ from 'lodash';
import PropTypes from 'prop-types';
import TableBody from './tableBody';

class Weather extends React.Component {
  componentDidMount() {
    const { resortId, getWeatherAction } = this.props;
    getWeatherAction(resortId);
  }

  render() {
    const { isLoaded, weather, forecastPast } = this.props;
    if (!isLoaded) {
      return <div>Loading...</div>;
    }
    if (weather.error) {
      return <div>Error: {weather.error}</div>;
    }
    if (forecastPast.error) {
      return <div>Error: {forecastPast.error}</div>;
    }
    return (
      <div id="current" className="row flex-column">
        <h1>Current weather report</h1>
        <p>
          <i>Last updated on {_.get(weather.payload, 'date_request')}</i>
        </p>
        <table className="table table-borderless">
          <thead>
            <tr>
              <th scope="col"> </th>
              <th scope="col" className="curr">
                <h5>
                  <b>Current</b>
                </h5>
              </th>
              <th scope="col"> </th>
              <th scope="col" colSpan="3">
                <h5>
                  <b>Previous predictions</b>
                </h5>
              </th>
            </tr>
            <tr>
              <th scope="col"> </th>
              <th scope="col" className="curr">
                <h5>
                  <b>Weather Report</b>
                </h5>
              </th>
              <th scope="col"> </th>
              <th scope="col" className="prev-1">
                <h5>
                  <b>-1</b>
                </h5>
              </th>
              <th scope="col" className="prev-3">
                <h5>
                  <b>-3</b>
                </h5>
              </th>
              <th scope="col" className="prev-7">
                <h5>
                  <b>-7</b>
                </h5>
              </th>
            </tr>
          </thead>
          <TableBody weather={weather.payload} forecastPast={forecastPast.payload} />
        </table>
      </div>
    );
  }
}

Weather.propTypes = {
  resortId: PropTypes.number.isRequired,
  isLoaded: PropTypes.bool.isRequired,
  weather: PropTypes.shape({
    isLoaded: PropTypes.bool.isRequired,
    error: PropTypes.string,
    payload: PropTypes.shape({
      id: PropTypes.number,
      resort_id: PropTypes.number,
      date_request: PropTypes.string,
      dt: PropTypes.string,
      date: PropTypes.string,
      timepoint: PropTypes.number,
      temperature_c: PropTypes.number,
      wind_speed_kmh: PropTypes.number,
      wind_direction_deg: PropTypes.number,
      visibility_km: PropTypes.number,
      clouds_pct: PropTypes.number,
      snow_3h_mm: PropTypes.number,
      rain_3h_mm: PropTypes.number,
    }),
  }).isRequired,
  forecastPast: PropTypes.shape({
    isLoaded: PropTypes.bool.isRequired,
    error: PropTypes.string,
    payload: PropTypes.arrayOf(
      PropTypes.shape({
        id: PropTypes.number,
        resort_id: PropTypes.number,
        date_request: PropTypes.string,
        date: PropTypes.string,
        timepoint: PropTypes.number,
        temperature_max_c: PropTypes.number,
        temperature_min_c: PropTypes.number,
        rain_total_mm: PropTypes.number,
        snow_total_mm: PropTypes.number,
        prob_precip_pct: PropTypes.number,
        wind_speed_max_kmh: PropTypes.number,
        windgst_max_kmh: PropTypes.number,
      }),
    ),
  }).isRequired,
  getWeatherAction: PropTypes.func.isRequired,
};

export default Weather;
