// Copyright (c) 2021. Michael Kemna.

import { MapLayer, withLeaflet } from 'react-leaflet';
import HeatmapOverlay from 'leaflet-heatmap';

const staticConfig = {
  radius: 2,
  minOpacity: 0,
  maxOpacity: 0.5,
  scaleRadius: true,
  useLocalExtrema: false,
  gradient: {
    '.3': 'darkblue',
    '.7': 'slateblue',
    '.99': 'ivory',
  },
  latField: 'lat',
  lngField: 'lng',
  valueField: `${window.REACT_APP_SNOW_OR_RAIN}_total_mm`,
};

class HeatmapLayer extends MapLayer {
  // eslint-disable-next-line class-methods-use-this
  createLeafletElement(_props) {
    const heatmap = new HeatmapOverlay(staticConfig);
    heatmap.setData({ max: _props.max, data: _props.data });
    return heatmap;
  }

  updateLeafletElement(_fromProps, _toProps) {
    this.leafletElement.setData({ max: _toProps.max, data: _toProps.data });
  }
}

export default withLeaflet(HeatmapLayer);
