// Copyright (c) 2021. Michael Kemna.

import React from 'react';
import './style.css';
import { CircleMarker, FeatureGroup, Tooltip } from 'react-leaflet';
import PropTypes from 'prop-types';
import ResortPopup from '../../containers/ResortPopup.container';

function Markers({ resortId, data, dynamicConfig, selectResortAction }) {
  // The Markers with the tooltip are always rendered
  // The Popup is only shown when one of the markers is clicked
  return (
    <FeatureGroup>
      {data.map((resort) => (
        <CircleMarker
          key={resort.id}
          center={[resort.lat, resort.lng]}
          onclick={() => {
            selectResortAction(resort.id);
          }}
          weight={1}
          color="wheat"
          {...dynamicConfig}
        >
          <Tooltip>
            <h6>
              <b>{resort.snow_week_mm} mm</b> snow in {resort.village}
            </h6>
          </Tooltip>
        </CircleMarker>
      ))}
      {resortId && <ResortPopup resortId={resortId} />}
    </FeatureGroup>
  );
}

Markers.propTypes = {
  resortId: PropTypes.number,
  data: PropTypes.arrayOf(
    PropTypes.exact({
      id: PropTypes.number.isRequired,
      village: PropTypes.string.isRequired,
      lat: PropTypes.number.isRequired,
      lng: PropTypes.number.isRequired,
      rain_week_mm: PropTypes.number.isRequired,
      snow_week_mm: PropTypes.number.isRequired,
    }),
  ).isRequired,
  dynamicConfig: PropTypes.exact({
    radius: PropTypes.number.isRequired,
    opacity: PropTypes.number.isRequired,
    fillOpacity: PropTypes.number.isRequired,
  }).isRequired,
  selectResortAction: PropTypes.func.isRequired,
};

Markers.defaultProps = {
  resortId: null,
};

export default Markers;
