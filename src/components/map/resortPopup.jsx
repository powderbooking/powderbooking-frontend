// Copyright (c) 2021. Michael Kemna.

import React from 'react';
import '../resort/style.css';
import { Link } from 'react-router-dom';
import { Popup } from 'react-leaflet';
import PropTypes from 'prop-types';
import ForecastDetails from '../../containers/ForecastDetails.container';
import ResortDetails from '../resort/resortDetails';

function ResortPopupButton({ url, title }) {
  return (
    <div id="div-btn" className="col-md-6">
      <Link to={url}>
        <div className="p-1 border border-primary rounded">
          <h5 className="m-auto text-center">{title}</h5>
        </div>
      </Link>
    </div>
  );
}

ResortPopupButton.propTypes = {
  url: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
};

function ResortPopupButtons({ resortId }) {
  return (
    <div className="row mt-2">
      <ResortPopupButton title="Find the best offer" url="#" />
      <ResortPopupButton title="Complete forecast" url={`/resort/${resortId}`} />
    </div>
  );
}

ResortPopupButtons.propTypes = {
  resortId: PropTypes.number.isRequired,
};

function ResortPopup({ resortId, error, isLoaded, payload, getResortAction }) {
  const resort = payload;
  return (
    <Popup minWidth={800} onOpen={() => getResortAction(resortId)}>
      {!isLoaded && <div>Loading...</div>}
      {isLoaded && error && <div>Error: {error.message}</div>}
      {isLoaded && !error && (
        <>
          <div className="row flex-column">
            <h3> {resort.village} </h3>
            <h5>
              {' '}
              {resort.continent} / {resort.country}{' '}
            </h5>
          </div>
          <div className="row">
            <div className="col-md-6">
              <ResortDetails resort={resort} />
            </div>
            <ForecastDetails resortId={resortId} />
          </div>
          <ResortPopupButtons resortId={resortId} />
        </>
      )}
    </Popup>
  );
}

ResortPopup.propTypes = {
  resortId: PropTypes.number.isRequired,
  error: PropTypes.shape({
    message: PropTypes.string,
  }),
  isLoaded: PropTypes.bool.isRequired,
  payload: PropTypes.exact({
    id: PropTypes.number,
    continent: PropTypes.string,
    name: PropTypes.string,
    fullname: PropTypes.string,
    country: PropTypes.string,
    village: PropTypes.string,
    lat: PropTypes.number,
    lng: PropTypes.number,
    altitude_min_m: PropTypes.number,
    altitude_max_m: PropTypes.number,
    lifts: PropTypes.number,
    slopes_total_km: PropTypes.number,
    slopes_blue_km: PropTypes.number,
    slopes_red_km: PropTypes.number,
    slopes_black_km: PropTypes.number,
  }),
  getResortAction: PropTypes.func.isRequired,
};

ResortPopup.defaultProps = {
  error: {},
  payload: {},
};

export default ResortPopup;
