// Copyright (c) 2021. Michael Kemna.

import React from 'react';
import './style.css';
import { Map as LeafletMap, TileLayer } from 'react-leaflet';
import PropTypes from 'prop-types';
import HeatmapLayer from './HeatmapLayer';
import Markers from '../../containers/Markers.container';

const staticConfig = {
  map: {
    viewport: {
      center: [31.0, 8.0],
      zoom: 3,
    },
    minZoom: 2,
    maxZoom: 12,
    maxBounds: [
      [85.0, -230.0], // bounds north east
      [-85.0, 230.0], // bounds south west
    ],
  },
  tileLayer: {
    url: 'https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}',
    id: 'mapbox/dark-v10',
    accessToken:
      'pk.eyJ1IjoibXJhc2FwIiwiYSI6ImNqazd6dHpxajI4YWozcG8yeWNpZXUybnEifQ.bp0IflatrzCyKPJDJwT74A',
    attribution:
      'Map &copy; <a href="https://www.mapbox.com/">Mapbox</a> | ' +
      'Weather &copy; <a href="http://www.weatherunlocked.com/">Weather Unlocked</a>',
  },
};

class Map extends React.Component {
  // TODO: switch to componentWillMount?
  componentDidMount() {
    const { getOverviewAction } = this.props;
    getOverviewAction();
    window.scrollTo(0, 0);
  }

  render() {
    const { data, max, handleViewportChangeAction } = this.props;
    return (
      <LeafletMap {...staticConfig.map} onViewportChanged={handleViewportChangeAction}>
        <TileLayer {...staticConfig.tileLayer} />
        {data && data.length > 0 && (max || max === 0) && (
          <>
            <HeatmapLayer max={max} data={data} />
            <Markers />
          </>
        )}
      </LeafletMap>
    );
  }
}

Map.propTypes = {
  data: PropTypes.arrayOf(
    PropTypes.exact({
      id: PropTypes.number,
      village: PropTypes.string,
      lat: PropTypes.number,
      lng: PropTypes.number,
      rain_week_mm: PropTypes.number,
      snow_week_mm: PropTypes.number,
    }),
  ),
  max: PropTypes.number,
  handleViewportChangeAction: PropTypes.func.isRequired,
  getOverviewAction: PropTypes.func.isRequired,
};

Map.defaultProps = {
  data: [],
  max: 0,
};

export default Map;
