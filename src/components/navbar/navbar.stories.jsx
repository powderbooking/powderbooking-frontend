// Copyright (c) 2021. Michael Kemna.

import React from 'react';
import { MemoryRouter } from 'react-router';
import Navbar from './navbar';

export default {
  title: 'Navbar/bar',
  component: Navbar,
};

const Template = (args) => (
  <MemoryRouter>
    <Navbar {...args} />
  </MemoryRouter>
);

export const MultipleRoutes = Template.bind({});
MultipleRoutes.args = {
  links: [
    {
      id: 0,
      title: 'map view',
      url: '/map',
      isRoute: true,
    },
    {
      id: 1,
      title: 'forecast',
      url: '#forecast',
      isRoute: false,
    },
    {
      id: 2,
      title: 'forecast',
      url: '#forecast',
      isRoute: false,
      requiredPath: 'not-the-path-so-it-isnt-rendered',
    },
  ],
};
