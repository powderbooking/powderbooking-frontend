// Copyright (c) 2021. Michael Kemna.

import React from 'react';
import './style.css';
import PropTypes from 'prop-types';
import NavbarItem from './navbarItem';

function Navbar({ links }) {
  return (
    <nav id="header" className="navbar navbar-expand-lg navbar-dark fixed-top">
      <a className="navbar-brand" href="/">
        Welcome powder hunter!
      </a>
      <button
        className="navbar-toggler"
        type="button"
        data-toggle="collapse"
        data-target="#navbarNavDropdown"
        aria-controls="navbarNavDropdown"
        aria-expanded="false"
        aria-label="Toggle navigation"
      >
        <span className="navbar-toggler-icon" />
      </button>
      <div className="collapse navbar-collapse justify-content-between" id="navbarNavDropdown">
        <ul className="navbar-nav">
          {links.map((link) => (
            <NavbarItem key={link.id.toString()} {...link} />
          ))}
        </ul>
      </div>
    </nav>
  );
}

Navbar.propTypes = {
  /**
   * A list of links that are to be displayed in the navbar.
   */
  links: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number.isRequired,
      title: PropTypes.string.isRequired,
      url: PropTypes.string.isRequired,
      isRoute: PropTypes.bool.isRequired,
      component: PropTypes.oneOfType([PropTypes.func, PropTypes.object]),
      requiredPath: PropTypes.string,
    }),
  ).isRequired,
};

export default Navbar;
