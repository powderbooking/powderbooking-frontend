// Copyright (c) 2021. Michael Kemna.

import React from 'react';
import { MemoryRouter } from 'react-router';
import NavbarItem from './navbarItem';

export default {
  title: 'Navbar/Item',
  component: NavbarItem,
};

const Template = (args) => (
  <MemoryRouter>
    <NavbarItem {...args} />
  </MemoryRouter>
);

export const Route = Template.bind({});
Route.args = {
  title: 'map view',
  url: '/map',
  isRoute: true,
};

export const Section = Template.bind({});
Section.args = {
  title: 'forecast',
  url: '#forecast',
  isRoute: false,
};

export const RequiredPath = Template.bind({});
RequiredPath.args = {
  title: 'forecast',
  url: '#forecast',
  isRoute: false,
  requiredPath: 'not-the-path-so-it-isnt-rendered',
};
