// Copyright (c) 2021. Michael Kemna.

import { Link, withRouter } from 'react-router-dom';
import React from 'react';
import PropTypes from 'prop-types';

function NavbarItemRoute({ title, url }) {
  return (
    <li className="nav-item">
      <Link to={url} className="nav-link">
        {title}
      </Link>
    </li>
  );
}

NavbarItemRoute.propTypes = {
  title: PropTypes.string.isRequired,
  url: PropTypes.string.isRequired,
};

function NavbarItemRef({ active, title, url }) {
  return (
    <li className="nav-item">
      <a href={url} className={active ? 'nav-link active' : 'nav-link'}>
        {title}
      </a>
    </li>
  );
}

NavbarItemRef.propTypes = {
  active: PropTypes.bool,
  title: PropTypes.string.isRequired,
  url: PropTypes.string.isRequired,
};

NavbarItemRef.defaultProps = {
  active: false,
};

function NavbarItem({ isRoute, location, requiredPath, title, url }) {
  if (requiredPath && !location.pathname.startsWith(requiredPath)) {
    // This item shouldn't be rendered as the user is currently not on the page that is required
    return null;
  }
  if (isRoute && !location.pathname.startsWith(url.split(':')[0])) {
    // We are on another page, so clicking the item should bring us to that route
    return <NavbarItemRoute title={title} url={url} />;
  }
  if (isRoute) {
    // We are on the page of the route, so clicking the item should bring us to the top
    return <NavbarItemRef title={title} url="#content" active />;
  }
  // The item is just a ref to an id
  return <NavbarItemRef title={title} url={url} />;
}

NavbarItem.propTypes = {
  /**
   * The object you get from Router.
   */
  location: PropTypes.shape({
    pathname: PropTypes.string,
  }).isRequired,
  /**
   * The title of the navbarItem, which will be displayed as child.
   */
  title: PropTypes.string.isRequired,
  /**
   * The url that will be followed when it is clicked on.
   */
  url: PropTypes.string.isRequired,
  /**
   * Whether it is a route or just a section.
   */
  isRoute: PropTypes.bool.isRequired,
  /**
   * The path needs to start with this string, otherwise it won't render.
   */
  requiredPath: PropTypes.string,
};

NavbarItem.defaultProps = {
  requiredPath: null,
};

export default withRouter(NavbarItem);
