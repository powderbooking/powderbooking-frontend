#!/bin/sh -eu

# Copyright (c) 2021. Michael Kemna.

cat <<EOF
window.REACT_APP_BACKEND_API='${REACT_APP_BACKEND_API}';
window.REACT_APP_SNOW_OR_RAIN='snow';
EOF