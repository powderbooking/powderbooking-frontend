#!/bin/sh -eu

# Copyright (c) 2021. Michael Kemna.

# based on: https://blog.codecentric.de/en/2018/12/react-application-container-environment-aware-kubernetes-deployment/
./generate_config_js.sh > /usr/share/nginx/html/config.js

echo "Starting app from entrypoint with args: $*"

# reuse original entrypoint: https://github.com/nginxinc/docker-nginx/blob/master/mainline/alpine/Dockerfile
exec ./docker-entrypoint.sh "$@"